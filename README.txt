Serviço de Difusão de Mensagens Curtas
  - Rui Ribeiro (pg16448)
  - Edgar Costa (pg16447)

Mensagens:
    - O protocolo tem uma natureza "pluggable" podendo ser modificado sem alterar o código das classes genéricas (strategy pattern).

    - COMANDO<RS>MENSAGEM<RS>ENDERECO<RS>PORTA<EOT>

      - LOAD<RS>LOAD<RS>OPCIONAL<RS>OPCIONAL<EOT>
        * Pedido de informação de carga ao shortmessage-server
      
      - LOADREPLY<RS>CLIENT_COUNT<RS>OPCIONAL<RS>PORTA_LIGACAO<EOT>
        * O receive-cliente liga-se ao servidor selecionado na porta definida em PORTA_LIGACAO

      - BROADCAST<RS>MENSAGEM<RS>OPCIONAL<RS>OPCIONAL<EOT>
        * Pedido por parte do send-client para disseminar a mensagem
        
      - MESSAGE<RS>MENSAGEM<RS>OPCIONAL<RS>OPCIONAL<EOT>
        * Dirigida ao receive-client indica a chegada de uma mensagem
      
      - PING<RS>HELLO<RS>OPCIONAL<RS>OPCIONAL<EOT>
        * Pacote keep-alive do receive-cliente para shortmessage-server
      
      - PONG
        * Pacote resposta keep-alive do shortmessage-server ao receive-client
        
Processo de comunicação:
    1 - receive-client emite um pedido de carga (LOAD), aguarda uma resposta (LOADREPLY) dos servidores ativos;
    2 - Liga-se ao servidor com menos carga e emite um pacote keep-alive (PING, 5s);
    3 - Aguarda uma resposta por parte do shortmessage-server (PONG);
    4 - send-client emite para os servidores uma mensagem (BROADCAST);
    5 - os servidores (shortmessage-server) encaminham a mensagem para os seus clientes (MESSAGE);
    
Considerações:
  - shortmessage-server escutam na porta 3789 (TCP) por isso só é possível um por máquina;
  - receive-client escutam na porta 3790 (UDP), mas logo que a ligação se realiza a porta é libertada;
    * Após a ligação é possível arrancar mais receive-client;
  - send-client não tem limitação quanto ao número de aplicações abertas, uma vez que não escuta portas;
  - Dada a natureza do trabalho negligenciou-se proteção do pacote de dados e algum tratamento de erros;
  - Embora se tenha recorrido a threads, explorou-se sockets assíncronas (java.nio) num sistema orientado a eventos;
    * MulticastSocket mantém-se no padrão tradicional, apenas no Java 7 é que será suportada de forma assíncrona;
  - Tentou-se implementar um conjunto de padrões
    - Observer
    - Reactor
    - Strategy
    - EventQueue
  - O background em Java não é forte, algumas práticas podem não ser as melhores. Agradecem-se críticas e orientação para boas práticas.
  - Os jar devidamente compilados encontram-se na pasta pdtr-tpi/out