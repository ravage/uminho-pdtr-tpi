package ers.mi.uminho.sendclient;

import ers.mi.uminho.protocol.ShortMessageProtocol;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 13/03/2011
 * Time: 14:24
 * To change this template use File | Settings | File Templates.
 */
public class App {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.out.println("Going Swing default!");
        }
        SwingUtilities.invokeLater(new SendClient("228.1.2.3", 6789, new ShortMessageProtocol()));
    }
}
