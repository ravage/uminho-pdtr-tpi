package ers.mi.uminho.sendclient;

import ers.mi.uminho.components.IProtocol;
import ers.mi.uminho.multicast.messenger.MulticastMessenger;
import ers.mi.uminho.protocol.ShortMessagePacket;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 13/03/2011
 * Time: 14:12
 * To change this template use File | Settings | File Templates.
 */
public class SendClient implements Runnable, ActionListener, KeyListener {
    private JTextArea txtMessage;
    private JButton btnSend;
    private JPanel panel;
    private JTextField txtGroup;
    private JTextField txtPort;
    private JLabel lblNotification;
    private MulticastMessenger messenger;

    public SendClient(String group, Integer port, IProtocol protocol) {
        txtPort.setEnabled(false);
        txtGroup.setEnabled(false);
        txtGroup.setText(group);
        txtPort.setText(port.toString());

        try {
            messenger = new MulticastMessenger(group, port, protocol);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        bindListeners();
    }

    public void run() {
        txtMessage.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY),
                BorderFactory.createEmptyBorder(3, 3, 3, 3)));
        txtGroup.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY),
                BorderFactory.createEmptyBorder(0, 3, 0, 0)));
        txtPort.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY),
                BorderFactory.createEmptyBorder(0, 3, 0, 0)));

        JFrame frame = new JFrame("Send Client");
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setAlwaysOnTop(false);
    }

    public void actionPerformed(ActionEvent e) {
        if (txtMessage.getText().isEmpty())
            return;

        ShortMessagePacket packet = new ShortMessagePacket("BROADCAST", txtMessage.getText());
        messenger.send(packet);
        txtMessage.setText("");
        lblNotification.setText("Data Sent!");
    }

    public void keyTyped(KeyEvent e) {
        JTextArea component = (JTextArea)e.getComponent();
        if (component.getText().length() >= 140)
            e.consume();

    }

    public void keyPressed(KeyEvent e) {
        JTextArea component = (JTextArea)e.getComponent();
        lblNotification.setText(Integer.toString(140 - component.getText().length()));
    }

    public void keyReleased(KeyEvent e) {
        keyPressed(e);
    }

    private void bindListeners() {
        btnSend.addActionListener(this);
        txtMessage.addKeyListener(this);
    }

}
