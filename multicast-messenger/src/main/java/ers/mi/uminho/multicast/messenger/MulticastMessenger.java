package ers.mi.uminho.multicast.messenger;

import ers.mi.uminho.components.IPacket;
import ers.mi.uminho.components.IProtocol;

import java.io.IOException;
import java.net.*;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 21/03/2011
 * Time: 14:33
 * To change this template use File | Settings | File Templates.
 */
public class MulticastMessenger {
    private final DatagramSocket socket;
    private final InetAddress group;
    private final int port;
    private final IProtocol<IPacket> protocol;

    public MulticastMessenger(String group, int port, IProtocol protocol) throws SocketException, UnknownHostException {
        socket = new DatagramSocket();
        this.port = port;
        this.group = InetAddress.getByName(group);
        this.protocol = protocol;
    }

    public void send(IPacket packet) {
        byte[] buffer = protocol.toByteArray(packet);
        DatagramPacket datagram = new DatagramPacket(buffer, buffer.length, group, port);
        try {
            socket.send(datagram);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
