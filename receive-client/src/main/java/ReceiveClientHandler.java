import ers.mi.uminho.components.EventQueue;
import ers.mi.uminho.components.IPacket;
import ers.mi.uminho.components.IServerEventListener;
import ers.mi.uminho.multicast.messenger.MulticastMessenger;
import ers.mi.uminho.protocol.ShortMessagePacket;
import ers.mi.uminho.protocol.ShortMessageProtocol;
import ers.mi.uminho.tcp.TCPClient;
import ers.mi.uminho.udp.UDPServer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SocketChannel;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 19/03/2011
 * Time: 16:14
 * To change this template use File | Settings | File Templates.
 */
public class ReceiveClientHandler extends Observable implements IServerEventListener {
    private final SortedMap<Integer, InetSocketAddress> responses;
    private final MulticastMessenger multicastMessenger;
    private final EventQueue eventQueue;
    private final UDPServer udpServer;
    private TCPClient tcpClient;
    private final SimpleDateFormat dateFormat;
    private Timer serverSelectionTimer;
    private Timer helloTimer;
    private static final long TIMEOUT = 5 * 1000;
    private WhatChanged state;
    private final HelloWorker helloWorker;
    private final ShortMessageProtocol protocol;

    public ReceiveClientHandler(String multicastGroup, int multicastPort, int udpPort) throws IOException {
        eventQueue = new EventQueue();
        multicastMessenger = new MulticastMessenger(multicastGroup, multicastPort, new ShortMessageProtocol());
        responses = new ConcurrentSkipListMap<Integer, InetSocketAddress>();
        udpServer = new UDPServer(eventQueue.getDispatcher(), udpPort, new ShortMessageProtocol());
        tcpClient = new TCPClient(eventQueue.getDispatcher(), new ShortMessageProtocol());
        dateFormat = new SimpleDateFormat("d/M/yyyy @ k:m:s");
        helloWorker = new HelloWorker();
        eventQueue.addServer(udpServer);
        eventQueue.addEventListener(this);
        state = WhatChanged.Idle;
        protocol = new ShortMessageProtocol();
    }

    public void connect() {
        eventQueue.startServers();
        serverSelectionTimer = new Timer();
        helloTimer = new Timer();
        discover();
    }

    public void readComplete(DatagramPacket datagramPacket, IPacket packet) {
    }

    public void writeComplete(DatagramPacket datagramPacket) {
    }

    public void writeComplete(SocketChannel channel, ByteBuffer buffer) {
    }

    public void readComplete(SocketChannel channel, IPacket packet) {
        if (packet.getCommand().equals("PONG")) {
            String response = String.format("[R][%s] %s from %s", dateFormat.format(new Date()), packet.getCommand(),
                    channel.socket().getInetAddress());

            setState(WhatChanged.Information, response);
        }
        else if (packet.getCommand().equals("MESSAGE")) {
            String response = String.format("[M][%s] %s", dateFormat.format(new Date()), packet.getMessage());
            setState(WhatChanged.Message, response);
        }
    }

    public void writeComplete(DatagramChannel channel, ByteBuffer buffer) {
    }

    public void readComplete(SocketAddress clientSocket, IPacket packet) {
        ShortMessagePacket data = (ShortMessagePacket) packet;
        InetSocketAddress client = (InetSocketAddress) clientSocket;
        if (packet.getCommand().equals("LOADREPLY")) {
            responses.put(Integer.parseInt(packet.getMessage()), new InetSocketAddress(client.getAddress(),
                    Integer.parseInt(packet.getPort())));
            String response = String.format("[R][%s] %s reports %s slots used", dateFormat.format(new Date()),
                    client, packet.getMessage());
            setState(WhatChanged.Information, response);
        }
    }

    public void readUnit(SocketChannel channel, ByteBuffer buffer) {
    }

    public void clientConnected(SocketChannel channel) {
    }

    public void connected(Socket endpoint) {
        String response = String.format("[N][%s] Connected to %s:%d", dateFormat.format(new Date()),
                endpoint.getInetAddress(), endpoint.getPort());
        setState(WhatChanged.Connect, response);
    }

    public void readUnit(DatagramChannel channel, ByteBuffer buffer) {
    }

    public void clientDisconnected(SocketChannel channel) {
        String response = String.format("[N][%s] Connection reseted by peer!", dateFormat.format(new Date()));
        setState(WhatChanged.Disconnect, response);
        disconnect();
    }

    public WhatChanged getState() {
        return state;
    }
    private <T> void setState(WhatChanged value, T data) {
        state =  value;
        setChanged();
        notifyObservers(data);
    }

    public void disconnect() {
        eventQueue.stopServers();
        helloTimer.cancel();
        helloTimer.purge();
        tcpClient.disconnect();
    }

    public enum WhatChanged {
        Connect,
        Disconnect,
        Information,
        Message,
        Idle
    }

    private void discover() {
        ShortMessagePacket packet = new ShortMessagePacket("LOAD", "LOAD");
        packet.setPort(String.valueOf(udpServer.getPort()));
        multicastMessenger.send(packet);
        String response = String.format("[N][%s] %s (%d ms)", dateFormat.format(new Date()),
                "Sent Load Request! Waiting Response...", TIMEOUT);

        setState(WhatChanged.Information, response);
        serverSelectionTimer.schedule(new ServerSelection(), TIMEOUT);
    }

    private class ServerSelection extends TimerTask {
        public void run() {
            if (responses.isEmpty()) {
                String response = String.format("[N][%s] No servers answered, retrying...", dateFormat.format(new Date()));
                setState(WhatChanged.Information, response);
                discover();
                return;
            }

            InetSocketAddress endpoint = responses.get(responses.firstKey());
            try {
                tcpClient.connect(endpoint);
                udpServer.stop();
                serverSelectionTimer.cancel();
                responses.clear();
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            helloTimer.schedule(new HelloWorker(), 5000, 5000);
        }
    }

    private class HelloWorker extends TimerTask {
        @Override
        public void run() {
            if (tcpClient.isConnected()) {
                //TODO: implement some kind of timeout if no reply if found
                ShortMessagePacket packet = new ShortMessagePacket("PING", "HELLO");
                tcpClient.send(packet);
            }
        }
    }
}
