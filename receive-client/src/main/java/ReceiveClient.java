import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 13/03/2011
 * Time: 19:19
 * To change this template use File | Settings | File Templates.
 */
public class ReceiveClient implements Runnable, ActionListener, Observer {
    private JTextField txtUdpPort;
    private JList lstLog;
    private JTextField txtMulticastGroup;
    private JPanel panel;
    private JButton btnConnect;
    private JButton btnDisconnect;
    private JTable lstMessageLog;
    private JTable lstSystemLog;
    private JScrollPane jsMessageLog;
    private JScrollPane jsSystemLog;
    private JTextField txtMulticastPort;

    private final DefaultTableModel messageLog;
    private final DefaultTableModel systemLog;
    private final ReceiveClientHandler handler;

    public ReceiveClient(String multicastGroup, int multicastPort, int udpPort) throws IOException {
        messageLog = new DefaultTableModel();
        systemLog = new DefaultTableModel();
        lstSystemLog.setModel(systemLog);
        lstMessageLog.setModel(messageLog);
        handler = new ReceiveClientHandler(multicastGroup, multicastPort, udpPort);
        handler.addObserver(this);
        txtMulticastPort.setText(String.valueOf(multicastPort));
        txtMulticastGroup.setText(multicastGroup);
        txtUdpPort.setText(String.valueOf(udpPort));
        bindActionListeners();
    }

    public void run() {
        txtMulticastGroup.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY),
               BorderFactory.createEmptyBorder(0, 3, 0, 0)));

        txtUdpPort.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY),
                BorderFactory.createEmptyBorder(0, 3, 0, 0)));

        txtMulticastPort.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY),
                BorderFactory.createEmptyBorder(0, 3, 0, 0)));

        messageLog.addColumn("Messages");
        systemLog.addColumn("System Log");

        jsSystemLog.setAutoscrolls(true);
        jsMessageLog.setAutoscrolls(true);
        btnDisconnect.setEnabled(false);
        JFrame frame = new JFrame("Receive Client");
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        //frame.setResizable(false);
        //frame.setSize(560, 400);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setAlwaysOnTop(false);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Disconnect")) {
            btnDisconnect.setEnabled(false);
            btnConnect.setEnabled(true);
            handler.disconnect();
        }
        else if (e.getActionCommand().equals("Connect")) {
            btnConnect.setEnabled(false);
            handler.connect();
        }
    }
    private void bindActionListeners() {
        btnConnect.addActionListener(this);
        btnDisconnect.addActionListener(this);
    }


    public void update(Observable o, Object arg) {
        if (handler.getState() == ReceiveClientHandler.WhatChanged.Information) {
            logSystem(arg);
        }
        else if (handler.getState() == ReceiveClientHandler.WhatChanged.Connect) {
            logSystem(arg);
            btnConnect.setEnabled(false);
            btnDisconnect.setEnabled(true);
        }
        else if (handler.getState() == ReceiveClientHandler.WhatChanged.Disconnect) {
            logSystem(arg);
            btnConnect.setEnabled(true);
            btnDisconnect.setEnabled(false);
        }
        else if (handler.getState() == ReceiveClientHandler.WhatChanged.Message) {
            logMessage(arg);
        }
    }

    private <T> void logMessage(T message) {
        messageLog.addRow(new Object[]{message});
        lstMessageLog.scrollRectToVisible(lstMessageLog.getCellRect(lstMessageLog.getRowCount(),
                lstMessageLog.getColumnCount(), true));
    }

    private <T> void logSystem(T message) {
        systemLog.addRow(new Object[] {message});
        lstSystemLog.scrollRectToVisible(lstSystemLog.getCellRect(lstSystemLog.getRowCount(),
                lstSystemLog.getColumnCount(), true));
    }
}
