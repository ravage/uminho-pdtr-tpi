package ers.mi.uminho;

import javax.swing.*;
import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 05/03/2011
 * Time: 19:28
 * To change this template use File | Settings | File Templates.
 */
public class App {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.out.println("Going Swing default!");
        }
        try {
            ShortMessageServerGUI gui = new ShortMessageServerGUI("228.1.2.3", 6789, 3789);
            SwingUtilities.invokeLater(gui);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
