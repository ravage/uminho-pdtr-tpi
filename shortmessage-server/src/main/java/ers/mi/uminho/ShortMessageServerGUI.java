package ers.mi.uminho;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 20/03/2011
 * Time: 16:17
 * To change this template use File | Settings | File Templates.
 */
public class ShortMessageServerGUI implements Runnable, ActionListener, Observer {
    private JPanel panel;
    private JTable tblLog;
    private JButton btnStart;
    private JLabel lblClients;
    private JTextField txtMulticastGroup;
    private JTextField txtMulticastPort;
    private JTextField txtTcpPort;

    private final DefaultTableModel tableModel;
    private final ShortMessageServer server;

    public ShortMessageServerGUI(String multicastGroup, int multicastPort, int tcpPort) throws IOException {
        btnStart.addActionListener(this);
        tableModel = new DefaultTableModel();
        server = new ShortMessageServer(multicastGroup, multicastPort, tcpPort);
        server.addObserver(this);
        txtMulticastGroup.setText(multicastGroup);
        txtMulticastPort.setText(String.valueOf(multicastPort));
        txtTcpPort.setText(String.valueOf(tcpPort));
    }

    public void run() {
        tblLog.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY),
                BorderFactory.createEmptyBorder(3, 3, 3, 3)));

        txtMulticastGroup.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY),
               BorderFactory.createEmptyBorder(0, 3, 0, 0)));

        txtMulticastPort.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY),
                BorderFactory.createEmptyBorder(0, 3, 0, 0)));

        txtTcpPort.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY),
                BorderFactory.createEmptyBorder(0, 3, 0, 0)));

        tblLog.setModel(tableModel);
        tblLog.setAutoCreateColumnsFromModel(true);
        tableModel.addColumn("Log");
        JFrame frame = new JFrame("Short Message Server");
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setAlwaysOnTop(false);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public void update(Observable o, Object arg) {
        tableModel.addRow(new Object[] {arg});
        tblLog.scrollRectToVisible(tblLog.getCellRect(tblLog.getRowCount(), tblLog.getColumnCount(), true));
        lblClients.setText(String.valueOf(server.getClientCount()));
    }

    public void actionPerformed(ActionEvent e) {
        server.start();
        btnStart.setEnabled(false);
    }
}
