package ers.mi.uminho;

import ers.mi.uminho.components.EventQueue;
import ers.mi.uminho.components.IPacket;
import ers.mi.uminho.components.IServerEventListener;
import ers.mi.uminho.multicast.MulticastServer;
import ers.mi.uminho.protocol.ShortMessagePacket;
import ers.mi.uminho.protocol.ShortMessageProtocol;
import ers.mi.uminho.tcp.TCPServer;
import ers.mi.uminho.udp.UDPServer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SocketChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Observable;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 08/03/2011
 * Time: 18:10
 * To change this template use File | Settings | File Templates.
 */
public class ShortMessageServer extends Observable implements IServerEventListener, Runnable {
    private final EventQueue eventQueue;
    private final TCPServer tcpServer;
    private final MulticastServer multicastServer;
    private final UDPServer udpServer;
    private final SimpleDateFormat dateFormat;

    public ShortMessageServer(String multicastGroup, int multicastPort, int tcpPort) throws IOException {
        eventQueue = new EventQueue();
        tcpServer = new TCPServer(eventQueue.getDispatcher(), tcpPort, new ShortMessageProtocol());
        multicastServer = new MulticastServer(eventQueue.getDispatcher(), multicastGroup, multicastPort, new ShortMessageProtocol());
        udpServer = new UDPServer(eventQueue.getDispatcher(), 0, new ShortMessageProtocol());
        dateFormat = new SimpleDateFormat("d/M/yyyy @ k:m:s");
    }

    public void start() {
        Thread t = new Thread(this);
        t.setName("Short Message Server");
        t.start();
        log("Waiting for requests....");
    }

    public void run() {
        eventQueue.addServer(tcpServer);
        eventQueue.addServer(multicastServer);
        eventQueue.addServer(udpServer);
        eventQueue.startServers();
        eventQueue.addEventListener(this);
    }

    public void readComplete(DatagramPacket datagramPacket, IPacket packet) {
        ShortMessagePacket message = (ShortMessagePacket) packet;
        if (message.getCommand().equals("LOAD"))  {
            ShortMessagePacket p = new ShortMessagePacket("LOADREPLY", String.valueOf(tcpServer.getClientCount()));
            p.setPort(tcpServer.getPort().toString());
            datagramPacket.setPort(Integer.parseInt(packet.getPort()));
            udpServer.send(datagramPacket.getSocketAddress(), p);
        }
        else if (message.getCommand().equals("BROADCAST")) {
            ShortMessagePacket p = new ShortMessagePacket("MESSAGE", message.getMessage());
            tcpServer.send(p);
        }
        log(packet);
    }

    private void log(IPacket packet) {
        String response = String.format("[%s] %s : %s", dateFormat.format(new Date()), packet.getCommand(),
                packet.getMessage());
        setState(response);
    }

    private void log(String message) {
        String response = String.format("[%s] %s", dateFormat.format(new Date()), message);
        setState(response);
    }

    public void writeComplete(DatagramPacket packet) {
    }

    public void writeComplete(SocketChannel channel, ByteBuffer buffer) {
    }

    public void readComplete(SocketChannel channel, IPacket data) {
        ShortMessagePacket packet = (ShortMessagePacket) data;
        if (packet.getCommand().equals("PING")) {
            ShortMessagePacket p = new ShortMessagePacket("PONG", "HELLO");
            tcpServer.send(channel, p);
        }
        log(packet);
    }

    public void writeComplete(DatagramChannel channel, ByteBuffer buffer) {
    }

    public void readComplete(SocketAddress finalClientSocket, IPacket packet) {
    }

    public void readUnit(SocketChannel channel, ByteBuffer buffer) {
    }

    public void clientConnected(SocketChannel clientChannel) {
        log(String.format("Client %s connected", clientChannel.socket().getInetAddress()));
    }

    public void connected(Socket endpoint) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void readUnit(DatagramChannel channel, ByteBuffer buffer) {
    }

    public void clientDisconnected(SocketChannel channel) {
        tcpServer.removeClient(channel);
        log(String.format("Client %s disconnected", channel.socket().getInetAddress()));
    }

    public int getClientCount() {
        return tcpServer.getClientCount();
    }

    public <T> void setState(T message) {
        setChanged();
        notifyObservers(message);
    }
}
