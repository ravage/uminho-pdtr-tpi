package ers.mi.uminho.protocol;

import ers.mi.uminho.components.IProtocol;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 09/03/2011
 * Time: 13:37
 * To change this template use File | Settings | File Templates.
 */

/*

 */
public class ShortMessageProtocol implements IProtocol<ShortMessagePacket> {
    private final StringBuffer buffer;
    private final char EOT;
    private final char RS;

    public ShortMessageProtocol() {
        buffer = new StringBuffer();
        EOT = '\u0004';
        RS = '\u001E';
    }

    public ByteBuffer encode(ShortMessagePacket message) {
        ByteBuffer encodedMessage = null;
        StringBuilder sb = new StringBuilder();
        sb.append(message.getCommand()).append(RS);
        sb.append(message.getMessage()).append(RS);
        sb.append(message.getAddress()).append(RS);
        sb.append(message.getPort()).append(RS);
        sb.append(EOT);
        CharsetEncoder encoder = Charset.forName("UTF-8").newEncoder();
        try {
            encodedMessage = encoder.encode(CharBuffer.wrap(sb));
        } catch (CharacterCodingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return encodedMessage;
    }

    public byte[] toByteArray(ShortMessagePacket message) {
        ByteBuffer data = encode(message);
        data.clear();
        byte[] bytes = new byte[data.capacity()];
        data.get(bytes, 0, bytes.length);
        return bytes;
    }

    public ShortMessagePacket decode(byte[] data) {
        return decode(ByteBuffer.wrap(data));
    }

    public ShortMessagePacket decode(ByteBuffer data) {
       CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
       String message = "";
        try {
            message = decoder.decode(data).toString();
        } catch (CharacterCodingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (!message.contains(String.valueOf(EOT))) {
            buffer.append(message.trim());
            return null;
        }
        buffer.append(message.trim());
        return rebuild();
    }

    private ShortMessagePacket rebuild() {
        String message = buffer.toString();
        String[] tokens = message.split(String.valueOf(RS));

        if (tokens.length < 4)
            return null;

        buffer.setLength(0);

        ShortMessagePacket packet = new ShortMessagePacket(tokens[0], tokens[1]);
        packet.setAddress(tokens[2]);
        packet.setPort(tokens[3]);

        return packet;
    }
}
