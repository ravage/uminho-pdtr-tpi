package ers.mi.uminho.protocol;

import ers.mi.uminho.components.IPacket;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 09/03/2011
 * Time: 16:59
 * To change this template use File | Settings | File Templates.
 */
public class ShortMessagePacket implements IPacket {
    private String command;
    private String message;
    private String port;
    private String address;

    public ShortMessagePacket(String command, String message) {
        this.command = command;
        this.message = message;
        address = "";
        port = "0";
    }

    public String getCommand() {
        return command;
    }

    public String getMessage() {
        return message;
    }

    public String getPort() {
        return port;
    }

    public String getAddress() {
        return address;
    }

    public void setCommand(String value) {
        command = value;
    }

    public void setMessage(String value) {
        message = value;
    }

    public void setPort(String value) {
        port = value;
    }

    public void setAddress(String value) {
        address = value;
    }
}
