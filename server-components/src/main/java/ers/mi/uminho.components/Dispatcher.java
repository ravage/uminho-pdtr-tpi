package ers.mi.uminho.components;

import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 07/03/2011
 * Time: 01:15
 * To change this template use File | Settings | File Templates.
 */
public class Dispatcher {
    private final EventQueue eventQueue;
    private final Selector selector;

    public Dispatcher(EventQueue eventQueue) {
        this.eventQueue = eventQueue;
        selector = eventQueue.getSelector();
    }

    public void dispatch(Set<SelectionKey> keys) {
        Iterator iterator = keys.iterator();

        while (iterator.hasNext()) {
            SelectionKey key = (SelectionKey)iterator.next();
            iterator.remove();
            int currentOptions = key.readyOps();
            key.interestOps(key.interestOps() & ~currentOptions);

            if (!key.isValid()) {
                key.cancel();
                continue;
            }

            Runnable handler = (Runnable)key.attachment();

            if (handler != null)
                handler.run();
            else
                key.cancel();
        }
    }

    public void registerInterest(final SelectableChannel channel, final int interest, final Runnable handler) {
        defer(new Runnable() {
            public void run() {
                try {
                    if (channel.isOpen())
                        channel.register(selector, interest, handler);
                } catch (ClosedChannelException e) {
                    e.printStackTrace();
                    //channel.keyFor(selector).cancel();
                }
            }
        });
    }

    public void changeInterest(final SelectableChannel channel, final int interest) {
        defer(new Runnable() {
            public void run() {
                channel.keyFor(selector).interestOps(interest);
            }
        });
    }

    public void cancelInterest(final SelectableChannel channel) {
        defer(new Runnable() {
            public void run() {
                SelectionKey key = channel.keyFor(selector);
                if  (key != null) {
                    channel.keyFor(selector).cancel();
                }
            }
        });
    }

    public void registerEvent(IEventHandler event) {
        eventQueue.queueEvent(event);
    }

    public Selector getSelector() {
        return selector;
    }

    private void defer(Runnable task) {
        eventQueue.queueTask(task);
        selector.wakeup();
    }
}
