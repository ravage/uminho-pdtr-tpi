package ers.mi.uminho.components;

import java.net.DatagramPacket;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SocketChannel;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 08/03/2011
 * Time: 23:11
 * To change this template use File | Settings | File Templates.
 */
public interface IServerEventListener {
    public void readComplete(DatagramPacket datagramPacket, IPacket packet);

    public void writeComplete(DatagramPacket datagramPacket);

    public void writeComplete(SocketChannel channel, ByteBuffer buffer);

    public void readComplete(SocketChannel channel, IPacket packet);

    public void writeComplete(DatagramChannel channel, ByteBuffer buffer);

    public void readComplete(SocketAddress clientSocket, IPacket packet);

    public void readUnit(SocketChannel channel, ByteBuffer buffer);

    void clientConnected(SocketChannel channel);

    void connected(Socket endpoint);

    void readUnit(DatagramChannel channel, ByteBuffer buffer);

    void clientDisconnected(SocketChannel channel);
}
