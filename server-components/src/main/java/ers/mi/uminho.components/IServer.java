package ers.mi.uminho.components;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 08/03/2011
 * Time: 18:35
 * To change this template use File | Settings | File Templates.
 */
public interface IServer extends Runnable {
    public void start();
    public void stop();
}
