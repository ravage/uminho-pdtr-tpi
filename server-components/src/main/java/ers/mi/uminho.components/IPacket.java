package ers.mi.uminho.components;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 09/03/2011
 * Time: 18:04
 * To change this template use File | Settings | File Templates.
 */
public interface IPacket {
    public String getCommand();
    public String getMessage();
    public String getPort();
    public String getAddress();

    public void setCommand(String value);
    public void setMessage(String value);
    public void setPort(String value);
    public void setAddress(String value);
}

