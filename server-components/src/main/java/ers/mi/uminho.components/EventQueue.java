package ers.mi.uminho.components;

import java.io.IOException;
import java.nio.channels.Selector;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 07/03/2011
 * Time: 01:03
 * To change this template use File | Settings | File Templates.
 */
public class EventQueue implements Runnable {
    private final Selector selector;
    private final Dispatcher dispatcher;
    private final ConcurrentLinkedQueue<Runnable> tasks;
    private final ConcurrentLinkedQueue<IServer> servers;
    private final ConcurrentLinkedQueue<IEventHandler> events;
    private final ConcurrentLinkedQueue<IServerEventListener> listeners;

    public EventQueue() throws IOException {
        selector = Selector.open();
        dispatcher = new Dispatcher(this);
        tasks = new ConcurrentLinkedQueue<Runnable>();
        servers = new ConcurrentLinkedQueue<IServer>();
        events = new ConcurrentLinkedQueue<IEventHandler>();
        listeners = new ConcurrentLinkedQueue<IServerEventListener>();
    }

    public void addEventListener(IServerEventListener listener) {
        listeners.add(listener);
    }

    public void removeEventListener(IServerEventListener listener) {
        listeners.remove(listener);
    }

    private void notifyListeners() {
        synchronized (events) {
            while(!events.isEmpty()) {
                final IEventHandler event = events.remove();
                for (final IServerEventListener listener : listeners) {
                    event.dispatch(listener);
                }
            }
        }
    }

    public void queueTask(Runnable task) {
        tasks.add(task);
        selector.wakeup();
    }

    public void queueEvent(IEventHandler event) {
        events.add(event);
        selector.wakeup();
    }

    public void addServer(IServer server) {
        servers.add(server);
    }

    public void startServers() {
        Thread thread = new Thread(this);
        thread.setName("Event Queue");
        thread.start();

        for (IServer server : servers) {
            server.start();
        }
    }

    public void stopServers() {
        for (IServer server : servers) {
            server.stop();
        }
    }

    public Dispatcher getDispatcher() {
        return dispatcher;
    }

    public void run() {
        while (!Thread.interrupted()) {
            runPendingTasks();
            notifyListeners();
            int keyCount = 0;

            try {
                keyCount = selector.select();

            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                continue;
            }

            if (keyCount == 0)
                continue;

            dispatcher.dispatch(getSelector().selectedKeys());
        }
    }

    private void runPendingTasks() {
        synchronized (tasks) {
            while (!tasks.isEmpty()) {
                tasks.remove().run();
            }
        }
    }

    public Selector getSelector() {
        return selector;
    }

}
