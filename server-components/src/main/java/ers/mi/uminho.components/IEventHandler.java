package ers.mi.uminho.components;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 19/03/2011
 * Time: 14:32
 * To change this template use File | Settings | File Templates.
 */
public interface IEventHandler {
    public void dispatch(IServerEventListener listener);
}
