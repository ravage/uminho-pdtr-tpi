package ers.mi.uminho.components;

import java.nio.ByteBuffer;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 07/03/2011
 * Time: 03:30
 * To change this template use File | Settings | File Templates.
 */
public interface IProtocol<T> {
    public ByteBuffer encode(T message);
    public byte[] toByteArray(T message);
    public T decode(ByteBuffer data);
    public T decode(byte[] data);
}
