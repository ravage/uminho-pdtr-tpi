package ers.mi.uminho.udp;

import ers.mi.uminho.components.Dispatcher;
import ers.mi.uminho.components.IPacket;
import ers.mi.uminho.components.IProtocol;
import ers.mi.uminho.components.IServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.DatagramChannel;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 18/03/2011
 * Time: 19:51
 * To change this template use File | Settings | File Templates.
 */
public class UDPServer implements IServer {
    private final Dispatcher dispatcher;
    private final int port;
    private final IProtocol<IPacket> protocol;
    private DatagramChannel channel;
    private CommunicationChannel communicationChannel;

    public UDPServer(Dispatcher dispatcher, int port, IProtocol protocol) throws IOException {
        this.dispatcher = dispatcher;
        this.port = port;
        this.protocol = protocol;
    }

    public  void send(SocketAddress client, IPacket data) {
        communicationChannel.send(client, data);
    }

    public void start() {
        try {
            channel = DatagramChannel.open();
            channel.configureBlocking(false);
            communicationChannel = new CommunicationChannel(this.dispatcher, this.channel, this.protocol);
            communicationChannel.run();
            if (port > 0)
                channel.socket().bind(new InetSocketAddress(port));
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void stop() {
        communicationChannel.unregister();
        try {
            channel.disconnect();
            channel.close();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void run() {
       //communicationChannel.run();
    }

    public int getPort() {
        return port;
    }
}
