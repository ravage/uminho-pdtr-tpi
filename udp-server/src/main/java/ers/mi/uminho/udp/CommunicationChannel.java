package ers.mi.uminho.udp;

import ers.mi.uminho.components.*;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 18/03/2011
 * Time: 19:52
 * To change this template use File | Settings | File Templates.
 */
public class CommunicationChannel implements Runnable {

    private final Dispatcher dispatcher;
    private final DatagramChannel channel;
    private final IProtocol<IPacket> protocol;
    private final DatagramReader reader;
    private final DatagramWriter writer;

    public CommunicationChannel(Dispatcher dispatcher, DatagramChannel channel, IProtocol<IPacket> protocol) throws IOException {
        this.dispatcher = dispatcher;
        this.channel = channel;
        this.protocol = protocol;
        this.reader = new DatagramReader(this);
        this.writer = new DatagramWriter(this);
    }

    /*public void send(IPacket data) {
        writer.send(data);
    }*/

    public void send(SocketAddress client, IPacket data) {
        writer.send(client, data);
    }

    public void run() {
        reader.run();
    }

    public void unregister() {
        dispatcher.cancelInterest(channel);
    }

    private class DatagramWriter implements Runnable {
        private ByteBuffer buffer;
        private final CommunicationChannel communicationChannel;
        private SocketAddress client;

        public DatagramWriter(CommunicationChannel communicationChannel) throws IOException {
            this.communicationChannel = communicationChannel;
            buffer = ByteBuffer.allocateDirect(1024 * 8);
        }

        public void run() {
            if (channel.isOpen())
                try {
                    if (buffer != null)
                        channel.send(buffer, client);
                } catch (IOException e) {
                    dispatcher.cancelInterest(channel);
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

            if (buffer.hasRemaining()) {
                registerWriteInterest();
            } else {
                dispatcher.registerEvent(new IEventHandler() {
                    public void dispatch(IServerEventListener listener) {
                        listener.writeComplete(channel, buffer);
                    }
                });
                //udpReader.resumeReading();
                //dispatcher.registerEvent(new UDPServerEvent(communicationChannel, UDPServerEventType.WriteComplete,
                  //      new UDPServerState<DatagramChannel, ByteBuffer>(channel, buffer)));
            }
        }

        public void send(IPacket data) {
            registerWriteInterest();
            buffer = protocol.encode(data);
        }

        public void send(SocketAddress client, IPacket data) {
            this.client = client;
            send(data);
        }

        private void registerWriteInterest() {
            dispatcher.registerInterest(channel, SelectionKey.OP_WRITE, this);
        }
    }

    private class DatagramReader implements Runnable {
        private final ByteBuffer buffer;
        private final CommunicationChannel communicationChannel;

        public DatagramReader(CommunicationChannel communicationChannel) throws IOException {
            this.communicationChannel = communicationChannel;
            buffer = ByteBuffer.allocateDirect(1024 * 8);
            registerReadInterest();
        }

        public void run() {
            SocketAddress clientSocket = null;
            int bytesRead = 0;
            buffer.clear();
            try {
                clientSocket = channel.receive(buffer);
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            if (clientSocket == null) {
                registerReadInterest();
                return;
            }
            buffer.flip();
            final IPacket packet = protocol.decode(buffer);
            registerReadInterest();
            final SocketAddress finalClientSocket = clientSocket;
            if (packet == null)
                dispatcher.registerEvent(new IEventHandler() {
                    public void dispatch(IServerEventListener listener) {
                        listener.readUnit(channel, buffer);
                    }
                });
                //dispatcher.registerEvent(new UDPServerEvent(communicationChannel, UDPServerEventType.ReadUnit,
                //        new UDPServerState<DatagramChannel, ByteBuffer>(channel, buffer)));
            else
                dispatcher.registerEvent(new IEventHandler() {
                    public void dispatch(IServerEventListener listener) {
                        listener.readComplete(finalClientSocket, packet);
                    }
                });
                //dispatcher.registerEvent(new UDPServerEvent(communicationChannel, UDPServerEventType.ReadComplete,
                  //      new UDPServerState<SocketAddress, IPacket>(clientSocket, packet)));
        }

        public void resumeReading() {
            registerReadInterest();
        }

        private void registerReadInterest() {
            dispatcher.registerInterest(channel, SelectionKey.OP_READ, this);
        }

    }
}
