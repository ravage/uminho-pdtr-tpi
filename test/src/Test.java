import ers.mi.uminho.protocol.ShortMessagePacket;
import ers.mi.uminho.protocol.ShortMessageProtocol;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.nio.ByteBuffer;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 08/03/2011
 * Time: 17:07
 * To change this template use File | Settings | File Templates.
 */
public class Test {
   public static void main(String[] args) throws IOException {
       MulticastSocket socket = new MulticastSocket(6789);
       ShortMessageProtocol protocol = new ShortMessageProtocol();
       ShortMessagePacket packet = new ShortMessagePacket("TESTE", "ORA TESTA!");
       ByteBuffer buffer =  protocol.encode(packet);
       byte[] message = new byte[buffer.remaining()];
       buffer.get(message);
       DatagramPacket p = new DatagramPacket(message, message.length,  InetAddress.getByName("228.1.2.3"), 6789);
       //for (int i = 0; i < 10000; i++)
       socket.send(p);
   }
}
