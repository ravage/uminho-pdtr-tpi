package ers.mi.uminho.tcp;

import ers.mi.uminho.components.*;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 07/03/2011
 * Time: 02:24
 * To change this template use File | Settings | File Templates.
 */
public class TCPServer implements IServer {
    private final Dispatcher dispatcher;
    private final ServerSocketChannel serverChannel;
    private final IProtocol protocol;
    private final ConcurrentHashMap<SocketChannel, CommunicationChannel> connectedClients;
    private String address;
    private int port;

    public TCPServer(Dispatcher dispatcher, int port, IProtocol protocol) throws IOException {
        this.dispatcher = dispatcher;
        serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);
        this.protocol = protocol;
        this.port = port;
        connectedClients = new ConcurrentHashMap<SocketChannel, CommunicationChannel>();
    }

    public TCPServer(Dispatcher dispatcher, String address, int port, IProtocol protocol) throws IOException {
        this(dispatcher, port, protocol);
        this.address = address;
    }

    public void send(SocketChannel client, IPacket data) {
        CommunicationChannel channel = connectedClients.get(client);
        channel.send(data);
    }

    public void send(IPacket data) {
        for (CommunicationChannel channel : connectedClients.values()) {
            channel.send(data);
        }
    }

    public String getAddress() {
        return  serverChannel.socket().getInetAddress().getHostAddress();
    }

    public Integer getPort() {
        return serverChannel.socket().getLocalPort();
    }

    public void removeClient(SocketChannel client) {
        connectedClients.remove(client);
    }

    public void start() {
        if (port <= 0)
            return;

        InetSocketAddress socketAddress;
        if (address != null)
            socketAddress = new InetSocketAddress(address, port);
        else
            socketAddress = new InetSocketAddress(port);

        try {
            serverChannel.socket().bind(socketAddress);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        registerAcceptInterest();
    }

    public void stop() {
        try {
            dispatcher.cancelInterest(serverChannel);
            serverChannel.socket().close();
            synchronized (connectedClients) {
                Iterator iterator =  connectedClients.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry<SocketChannel, CommunicationChannel> entry = (Map.Entry)iterator.next();
                    entry.getValue().unregister();
                    entry.getKey().close();
                    iterator.remove();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void run() {
        try {
            final SocketChannel clientChannel = serverChannel.accept();
            clientChannel.configureBlocking(false);

            connectedClients.putIfAbsent(clientChannel,
                    new CommunicationChannel(dispatcher, clientChannel, protocol));

            registerAcceptInterest();

            dispatcher.registerEvent(new IEventHandler() {
                public void dispatch(IServerEventListener listener) {
                    listener.clientConnected(clientChannel);
                }
            });

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void registerAcceptInterest() {
        dispatcher.registerInterest(serverChannel, SelectionKey.OP_ACCEPT, this);
    }

    public int getClientCount() {
        return connectedClients.size();
    }
}
