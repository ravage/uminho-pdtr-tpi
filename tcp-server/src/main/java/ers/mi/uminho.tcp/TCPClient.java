package ers.mi.uminho.tcp;

import ers.mi.uminho.components.*;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 19/03/2011
 * Time: 01:31
 * To change this template use File | Settings | File Templates.
 */
public class TCPClient  implements Runnable {
    private final Dispatcher dispatcher;
    private SocketChannel channel;
    private final IProtocol<IPacket> protocol;
    private CommunicationChannel communicationChannel;

    public TCPClient(Dispatcher dispatcher, IProtocol protocol) throws IOException {
        this.dispatcher = dispatcher;
        this.protocol = protocol;
    }

    public void connect(SocketAddress endpoint) throws IOException {
        this.channel = SocketChannel.open();
        this.channel.configureBlocking(false);
        registerConnectInterest();
        channel.connect(endpoint);
    }

    private void registerConnectInterest() {
        dispatcher.registerInterest(channel, SelectionKey.OP_CONNECT, this);
    }

    public boolean isConnected() {
        return channel.isConnected() && communicationChannel != null;
    }

    public void send(IPacket packet) {
        if (communicationChannel != null)
            communicationChannel.send(packet);
    }

    public void run() {
        try {
            channel.finishConnect();
        } catch (IOException e) {
            dispatcher.cancelInterest(channel);
            e.printStackTrace();
            return;
        }
        communicationChannel = new CommunicationChannel(this.dispatcher, this.channel, protocol);
        dispatcher.registerEvent(new IEventHandler() {
            public void dispatch(IServerEventListener listener) {
                listener.connected(channel.socket());
            }
        });
    }

    public void disconnect() {
        try {
            dispatcher.cancelInterest(channel);
            channel.close();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
