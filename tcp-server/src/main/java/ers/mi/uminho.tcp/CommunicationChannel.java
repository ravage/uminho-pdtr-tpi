package ers.mi.uminho.tcp;

import ers.mi.uminho.components.*;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 09/03/2011
 * Time: 02:10
 * To change this template use File | Settings | File Templates.
 */
public class CommunicationChannel {
    private final Dispatcher dispatcher;
    private final SocketChannel channel;
    private final PacketWriter writer;
    private final PacketReader reader;
    private final IProtocol<IPacket> protocol;
    //private final TCPServer server;

    public CommunicationChannel(Dispatcher dispatcher, SocketChannel channel, IProtocol<IPacket> protocol) {
        this.dispatcher = dispatcher;
        this.channel = channel;
        writer = new PacketWriter(this);
        reader = new PacketReader(this);
        this.protocol = protocol;
        //this.server = server;
    }

    public void send(IPacket packet) {
        writer.send(packet);
    }

    public void unregister() {
        dispatcher.cancelInterest(channel);
    }

    private class PacketWriter implements Runnable {
        private ByteBuffer buffer;
        private final CommunicationChannel communicationChannel;

        public PacketWriter(CommunicationChannel communicationChannel) {
            buffer = ByteBuffer.allocate(1024*8);
            this.communicationChannel = communicationChannel;
            registerWriteInterest();
        }

        public void run() {
            int bytesWritten = 0;
            try {
                if (channel.isConnected())
                    bytesWritten = channel.write(buffer);

                if (bytesWritten == -1) {
                    dispatcher.registerEvent(new IEventHandler() {
                        public void dispatch(IServerEventListener listener) {
                            listener.clientDisconnected(channel);
                        }
                    });
                    return;
                }

            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            if (buffer.hasRemaining()) {
                registerWriteInterest();
            }
            else {
                reader.resumeReading();
                dispatcher.registerEvent(new IEventHandler() {
                    public void dispatch(IServerEventListener listener) {
                        listener.writeComplete(channel, buffer);
                    }
                });
                //dispatcher.registerEvent(new TCPServerEvent(communicationChannel, TCPServerEventType.WriteComplete, new TCPServerState(channel, buffer)));
            }
        }

        private void registerWriteInterest() {
            dispatcher.registerInterest(channel, SelectionKey.OP_WRITE, this);
        }

        public void send(IPacket value) {
            registerWriteInterest();
            buffer = protocol.encode(value);
        }
    }

    private class PacketReader implements Runnable {
        private final ByteBuffer buffer;
        private final CommunicationChannel communicationChannel;

        public PacketReader(CommunicationChannel communicationChannel) {
            buffer = ByteBuffer.allocate(1024*8);
            this.communicationChannel = communicationChannel;
            registerReadInterest();
        }

        private void registerReadInterest() {
            dispatcher.registerInterest(channel, SelectionKey.OP_READ, this);
        }

        public void resumeReading() {
            registerReadInterest();
        }

        public void run() {
            buffer.clear();
            int bytesRead = 0;
            try {
                bytesRead = channel.read(buffer);
            } catch (IOException e) {
                dispatcher.registerEvent(new IEventHandler() {
                    public void dispatch(IServerEventListener listener) {
                        listener.clientDisconnected(channel);
                    }
                });
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return;
            }

            if (bytesRead == -1) {
                try {
                    dispatcher.cancelInterest(channel);
                    channel.close();
                    dispatcher.registerEvent(new IEventHandler() {
                        public void dispatch(IServerEventListener listener) {
                            listener.clientDisconnected(channel);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }

            if (bytesRead == 0) {
                registerReadInterest();
                return;
            }
            buffer.flip();
            final IPacket packet = protocol.decode(buffer);
            registerReadInterest();
            if (packet == null)
                 dispatcher.registerEvent(new IEventHandler() {
                     public void dispatch(IServerEventListener listener) {
                         listener.readUnit(channel, buffer);
                     }
                 });
                //dispatcher.registerEvent(new TCPServerEvent<CommunicationChannel>(communicationChannel, TCPServerEventType.ReadUnit, new TCPServerState<ByteBuffer>(channel, buffer)));
            else
                dispatcher.registerEvent(new IEventHandler() {
                    public void dispatch(IServerEventListener listener) {
                        listener.readComplete(channel, packet);
                    }
                });
                //dispatcher.registerEvent(new TCPServerEvent<CommunicationChannel>(communicationChannel, TCPServerEventType.ReadComplete, new TCPServerState<IPacket>(channel, packet)));
        }
    }
}
