package ers.mi.uminho.multicast;

import ers.mi.uminho.components.*;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 09/03/2011
 * Time: 23:36
 * To change this template use File | Settings | File Templates.
 */
public class CommunicationChannel implements Runnable {
    private final MulticastSocket socket;
    private final DatagramReader reader;
    private final DatagramWriter writer;
    private final Dispatcher dispatcher;
    private final IProtocol<IPacket> protocol;

    public CommunicationChannel(Dispatcher dispatcher, MulticastSocket socket, IProtocol protocol) throws IOException {
        this.socket = socket;
        this.dispatcher = dispatcher;
        this.protocol = protocol;
        this.reader = new DatagramReader(this);
        this.writer = new DatagramWriter(this);
    }

    public void send(DatagramPacket datagramPacket, IPacket packet) {
        writer.send(datagramPacket, packet);
    }

    public void run() {
        reader.run();
    }

    private class DatagramReader implements Runnable {
        private final byte[] buffer = new byte[1024 * 8];
        private final CommunicationChannel communicationChannel;

        public DatagramReader(CommunicationChannel communicationChannel) {
            this.communicationChannel = communicationChannel;
        }

        public void run() {
            while (true) {
                final DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                try {
                    socket.receive(packet);
                } catch (IOException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

                final IPacket dataPacket = protocol.decode(packet.getData());
                if (dataPacket != null) {
                    dispatcher.registerEvent(new IEventHandler() {
                        public void dispatch(IServerEventListener listener) {
                            listener.readComplete(packet, dataPacket);
                        }
                    });
                /*    dispatcher.registerEvent(new MulticastServerEvent(communicationChannel, MulticastServerEventType.ReadComplete,
                            new MulticastServerState<DatagramPacket, IPacket>(packet, dataPacket)));*/
                }
            }
        }
    }

    private class DatagramWriter implements Runnable {
        private byte[] buffer = new byte[1024 * 8];
        private final CommunicationChannel communicationChannel;
        private DatagramPacket datagramPacket;

        private DatagramWriter(CommunicationChannel communicationChannel) {
            this.communicationChannel = communicationChannel;
        }

        public void run() {
            try {
                socket.send(datagramPacket);
                dispatcher.registerEvent(new IEventHandler() {
                    public void dispatch(IServerEventListener listener) {
                        listener.writeComplete(datagramPacket);
                    }
                });
                /*dispatcher.registerEvent(new MulticastServerEvent(communicationChannel, MulticastServerEventType.WriteComplete,
                        new MulticastServerState<DatagramPacket, IPacket>(datagramPacket)));*/
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        public void send(DatagramPacket dpacket,  IPacket packet) {
            buffer = protocol.toByteArray(packet);
            datagramPacket = new DatagramPacket(buffer, 0, buffer.length, dpacket.getAddress(), dpacket.getPort());
            run();
        }
    }
}
