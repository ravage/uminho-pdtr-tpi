package ers.mi.uminho.multicast;

import ers.mi.uminho.components.Dispatcher;
import ers.mi.uminho.components.IPacket;
import ers.mi.uminho.components.IProtocol;
import ers.mi.uminho.components.IServer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 08/03/2011
 * Time: 16:15
 * To change this template use File | Settings | File Templates.
 */
public class MulticastServer implements IServer {
    // 3790
    private final int port;
    // 228.1.2.3
    private final InetAddress group;
    private final MulticastSocket socket;
    private final Dispatcher dispatcher;
    private final CommunicationChannel communicationChannel;
    private final IProtocol<IPacket> protocol;
    private final Thread thread;
    private boolean connected;

    public MulticastServer(Dispatcher dispatcher, String group, int port, IProtocol protocol) throws IOException {
        this.port = port;
        this.group = InetAddress.getByName(group);
        socket = new MulticastSocket(this.port);
        //socket.setLoopbackMode(false);
        this.protocol = protocol;
        this.dispatcher = dispatcher;
        communicationChannel = new CommunicationChannel(dispatcher, socket, protocol);
        connected = false;
        thread = new Thread(this);
    }

    public void send(DatagramPacket datagramPacket, IPacket data) {
        communicationChannel.send(datagramPacket, data);
    }

    public void send(IPacket data) {
        byte[] buffer = new byte[1024];
        DatagramPacket dpacket = new DatagramPacket(buffer, 0, buffer.length, group, port);
        communicationChannel.send(dpacket, data);
    }

    public void start() {
        if (!thread.isAlive())
            thread.start();

        try {
            socket.joinGroup(group);
            connected = true;
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void stop() {
        try {
            if (connected) {
                socket.leaveGroup(group);
                connected = false;
            }
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void run() {
        communicationChannel.run();
    }
}
